Spree::Product.class_eval do
  searchkick autocomplete: [:name, :english_name, :sku, :barcode], batch_size: 300

  def search_data
    json = {
      id: id,
      name: name,
      english_name: english_name,
      description: description,
      active: available?,
      created_at: created_at,
      updated_at: updated_at,
      barcode: barcode,
      sku: sku,
      price: price,
      currency: currency,
      conversions: orders.complete.count,
      taxon_ids: taxon_and_ancestors.map(&:id),
      taxon_names: taxon_and_ancestors.map(&:name)
    }

    Spree::Property.all.each do |prop|
      json.merge!(Hash[prop.name.downcase, property(prop.name)])
    end

    Spree::Taxonomy.joins(:translations).all.each do |taxonomy|
      json.merge!(Hash["#{taxonomy.name.downcase}_ids", taxon_by_taxonomy(taxonomy.id).map(&:id)])
    end
    json
  end

  def taxon_by_taxonomy(taxonomy_id)
    taxons.joins(:taxonomy).where(spree_taxonomies: { id: taxonomy_id })
  end

  def self.autocomplete(keywords)
    if keywords

      # jsonhash = Spree::Product.search(keywords, autocomplete: true, limit: 10, exist: { fields: [:name, :sku, :image, :slug, :english_name] }).map do |product|

      jsonhash = Spree::Product.search(keywords, autocomplete: true, limit: 10, where: search_where).map do |product|
        if Rails.env.production?
          items = { ming: product.name, sku: product.sku, image: product.variant_images.first.attachment.url(:mini), slug: product.slug, english_name: product.english_name }.compact
          # items = { name: product.name, sku: product.sku, slug: product.slug, english_name: product.english_name }.compact
        else
          items = { ming: product.name, sku: product.sku, slug: product.slug, english_name: product.english_name }.compact
        end
      end
      jsonhash = { products: jsonhash }

      jsonhash.dup.tap do |h|
        h[:products].uniq! do |h|
          h[:sku]
        end
      end

    else
      jsonhash = Spree::Product.search('*', search_where).map do |product|
        if Rails.env.production?
          items = { ming: product.name, sku: product.sku, image: product.variant_images.first.attachment.url(:mini), slug: product.slug, english_name: product.english_name, barcode: product.barcode }.compact

        else
          number = rand(0..99)
          items = { ming: product.name, sku: product.sku, slug: product.slug, english_name: product.english_name, brand: product.brand , image: product.variant_images.first.attachment.url(:small), quantity: number}.compact

        end
      end
      jsonhash = { products: jsonhash }
      jsonhash.dup.tap do |h|
        h[:products].uniq! do |h|
          h[:sku]
        end
      end
    end
  end

  def self.search_where
    {
      active: true,
      price: { not: nil }
    }
  end
end
