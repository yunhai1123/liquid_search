// Placeholder manifest file.
// the installer will append this file to the app vendored assets here: vendor/assets/javascripts/spree/frontend/all.js'
//= require_tree .
$(function() {
  // Bloodhound.noConflict();
  var products = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace,
    //   datumTokenizer: function (datum) {
    //      return Bloodhound.tokenizers.whitespace(datum.name);
    //  },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    limit: 10,
    cahce: false,
    prefetch: Spree.pathFor('autocomplete/products.json'),
    remote: {
      url: Spree.pathFor('autocomplete/products.json?keywords=%QUERY'),
      wildcard: '%QUERY',
      transform: function(response) {
        // console.log('transform', response);
        // Map the remote source JSON array to a JavaScript object array
        return response.products;
      }
    }
  });

  var promise = products.initialize();

  promise
    .done(function() {

    })
    .fail(function() {


    });
  // passing in `null` for the `options` arguments will result in the default
  // options being used






  // $('#keywords').typeahead({
  $('.typeahead').typeahead({
    minLength: 2,
    highlight: true
  }, {
    name: 'products',
    displayKey: function(suggestion) {
      // console.log('suggestion', suggestion);
      return suggestion.ming
    },
    limit: 5,
    source: products,
    templates: {
      // pending: [
      //   '<div class="loading-message">',
      //   '<img src="http://st2.india.com/wp-content/uploads/2014/07/ajax-loader.gif"/>',
      //   '搜索中...Searching...',
      //   '</div>'
      // ].join('\n'),
      // notFound: [
      //   '<div class="empty-message">',
      //
      //   'nothing...nothing...',
      //   '</div>'
      // ].join('\n'),

      suggestion: function(data) {

        // console.log("this is json" + data.items.name);

        // return '<div class="liquid-suggestions"><a class=\"liquid-search-link\" href="http://wayfong.com/products/' + data.items.slug + '"<h4><img src=\"' + data.items.image + '\" class=""/>   ' + data.items.ming + '     ' + data.items.english_name + ' - ' + data.items.sku + '</h4></a></div>'
        // return '<div class="liquid-suggestions"><a class=\"liquid-search-link\" href="http://localhost:3000/products/' + data.items.slug + '"<h4><img src=\"http://placehold.it/48x48\" class=""/>   ' + data.items.ming + '     ' + data.items.english_name + ' - ' + data.items.sku + '</h4></a></div>'
        //   return '<div class="liquid-suggestions"><a class=\"liquid-search-link\" href="http://localhost:3000/products/' + data.items.slug + '"<h4>' + data.items.name + '     ' + data.items.english_name + ' - ' + data.items.sku + '</h4></a></div>'

          return '<div class="liquid-suggestions"><a class=\"liquid-search-link\" href="http://wayfong.com/products/' + data.slug + '"<h4><img src=\"' + data.image + '\" class=""/>   ' + data.ming + '     ' + data.english_name + ' - ' + data.sku + '</h4></a></div>'

      }
    }
  }).bind('typeahead:selected', function(obj, datum) {
    window.location.href = "http://wayfong.com/products/" + datum.slug ;
  });
});
